# DevOpsNetology
Course DevOps from Netology

- **/.terraform/* - локальные папки и файлы в папке terraform
- *.tfstate - все файлы с расширением .tfstate
- *.tfstate.* - файлы в именах которых встречается .tfstate.
- crash.log - файл crash.log в этой директории
- *.tfvars - все файлы с расширением .tfvars
- override.tf - файл ovveride.tf в этой директории
- override.tf.json - файл override.tf.json в этой директории
- *_override.tf - все файлы заканчивающиеся на _override.tf
- *_override.tf.json - все файлы заканчивающиеся на _override.tf.json
- .terraformrc - файлы с расширением .terraformrc в этой директории
- terraform.rc - файл terraform.rc